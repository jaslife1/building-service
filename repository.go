package main

import (
	"context"

	"github.com/mongodb/mongo-go-driver/bson"
	"github.com/mongodb/mongo-go-driver/bson/primitive"
	"github.com/mongodb/mongo-go-driver/mongo"

	pb "gitlab.com/lazybasterds/alpaca/building-service/proto/building"
)

const (
	dbName             = "alpacadb"
	buildingCollection = "buildinginfo"
	floorCollection    = "floor"
)

//Repository is an interface for getting information from the database.
type Repository interface {
	GetBuildingInfo(ctx context.Context) (*pb.Building, error)
	GetAllFloors(ctx context.Context) ([]*pb.Floor, error)
	GetFloor(ctx context.Context, floorID string) (*pb.Floor, error)
	Close(ctx context.Context)
}

//BuildingRepository concrete implementation of the Repository interface.
type BuildingRepository struct {
	db *DBConnection
}

// GetBuildingInfo gets the information about the current building that is stored in the database
func (repo *BuildingRepository) GetBuildingInfo(ctx context.Context) (*pb.Building, error) {
	collection := repo.collection(buildingCollection)

	var result Building
	err := collection.FindOne(ctx, bson.M{}).Decode(&result)
	if err != nil {
		return nil, err
	}

	return &pb.Building{
		Id:          result.ID.Hex(),
		Name:        result.Name,
		Location:    result.Location,
		Description: result.Description,
		Image:       toString(&result.ImageList),
	}, nil
}

// GetAllFloors will get all the floors in the MongoDB server
// This will return a slice of pb.Floor for all the floors, if an error
// is encountered it will return nil and the error
func (repo *BuildingRepository) GetAllFloors(ctx context.Context) ([]*pb.Floor, error) {
	collection := repo.collection(floorCollection)

	cur, err := collection.Find(ctx, bson.M{})
	if err != nil {
		return nil, err
	}
	defer cur.Close(ctx)

	var floors []Floor

	for cur.Next(ctx) {
		var floor Floor
		err := cur.Decode(&floor)
		if err != nil {
			return nil, err
		}

		floors = append(floors, floor)

	}
	if err := cur.Err(); err != nil {
		return nil, err
	}

	var results = make([]*pb.Floor, 0, len(floors))

	//Convert model to proto object
	for _, f := range floors {
		results = append(results, &pb.Floor{
			Id:   f.ID.Hex(),
			Name: f.Name,
		})
	}

	return results, err
}

// GetFloor will search the Database for floor specified by the floorID provided.
// This will return the Floor if the floor is found, otherwise it is nil
// If there are any error, it will return a nil together with the error.
func (repo *BuildingRepository) GetFloor(ctx context.Context, floorID string) (*pb.Floor, error) {
	collection := repo.collection(floorCollection)

	floorObjectID, err := primitive.ObjectIDFromHex(floorID)
	if err != nil {
		return nil, err
	}

	var result Floor
	err = collection.FindOne(ctx, bson.M{"_id": floorObjectID}).Decode(&result)

	if err != nil {
		return nil, err
	}

	return &pb.Floor{
		Id:   result.ID.Hex(),
		Name: result.Name,
	}, nil
}

//Close closes the session
func (repo *BuildingRepository) Close(ctx context.Context) {
	// TODO: Revisit if this is the correct context when closing session
	repo.db.session.EndSession(ctx)
}

func (repo *BuildingRepository) collection(coll string) *mongo.Collection {
	return repo.db.client.Database(dbName).Collection(coll)
}
