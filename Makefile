generate:
	protoc -I. --go_out=plugins=micro:. proto/building/building.proto

build: generate
	docker build -t building_service .
	# push to docker hub?

run:
	docker run -it \
		--rm --name "building_service" \
		-p 50052 \
		-e DB_HOST="mongodb://user:test123@alpaca-shard-00-00-cz5qu.mongodb.net:27017,alpaca-shard-00-01-cz5qu.mongodb.net:27017,alpaca-shard-00-02-cz5qu.mongodb.net:27017/test?ssl=true&replicaSet=Alpaca-shard-0&authSource=admin&retryWrites=true" \
		-e MICRO_SERVER_ADDRESS=:50052 \
		-e MICRO_REGISTRY=mdns \
		-e DISABLE_AUTH=true \
		building_service

release:
	docker build -t registry.gitlab.com/lazybasterds/alpaca/building-service:0.1.1 .
	docker push registry.gitlab.com/lazybasterds/alpaca/building-service:0.1.1