package main

import (
	"context"
	"log"
	"os"

	micro "github.com/micro/go-micro"
	_ "github.com/micro/go-plugins/registry/kubernetes"
	k8s "github.com/micro/kubernetes/go/micro"
	pb "gitlab.com/lazybasterds/alpaca/building-service/proto/building"
)

const (
	defaultHost = "localhost:27017"
)

var (
	srv micro.Service
)

func main() {
	host := os.Getenv("DB_HOST")

	if host == "" {
		host = defaultHost
	}

	ctx := context.Background()
	db, err := CreateDBConnection(ctx, host)

	if err != nil {
		// TODO: Wrap this in a good logging
		log.Panicf("Could not connect to the database with host %s - %v", host, err)
	}
	defer db.Close(ctx)

	srv = k8s.NewService(
		micro.Name("building-service"),
		micro.Version("latest"),
	)

	// Will parse the command line args
	srv.Init()

	// Register handler
	pb.RegisterBuildingServiceHandler(srv.Server(), &service{db})

	// Run the server
	if err := srv.Run(); err != nil {
		log.Printf("ERROR: Failed to run server for building-service - %v", err)
	}
}
