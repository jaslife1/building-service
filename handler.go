package main

import (
	"context"
	"log"

	"github.com/micro/go-micro/metadata"
	pb "gitlab.com/lazybasterds/alpaca/building-service/proto/building"
)

type service struct {
	db *DBConnection
}

func (s *service) GetRepo() Repository {
	return &BuildingRepository{s.db}
}

func (s *service) GetBuildingInfo(ctx context.Context, req *pb.Building, res *pb.BuildingResponse) error {
	log.Println(ctx)
	repo := s.GetRepo()
	defer repo.Close(ctx)

	mdata, ok := metadata.FromContext(ctx)

	var requestID string
	if ok {
		requestID = mdata["requestid"]
	}

	buildingInfo, err := repo.GetBuildingInfo(ctx)
	if err != nil {
		log.Printf("Error: Failed to get building information : %+v", err)
		return err
	}
	log.Printf("Method: GetBuildingInfo, RequestID: %s, Return: %#v", requestID, buildingInfo)
	res.Building = buildingInfo
	return nil
}

func (s *service) GetAllFloors(ctx context.Context, req *pb.Floor, res *pb.FloorResponse) error {
	log.Println(ctx)
	repo := s.GetRepo()
	defer repo.Close(ctx)

	mdata, ok := metadata.FromContext(ctx)

	var requestID string
	if ok {
		requestID = mdata["requestid"]
	}

	floors, err := repo.GetAllFloors(ctx)
	if err != nil {
		log.Printf("Error: Failed to get all floors : %+v", err)
		return err
	}
	log.Printf("Method: GetAllFloors, RequestID: %s, Return: %+v", requestID, floors)
	res.Floors = floors
	return nil
}

func (s *service) GetFloor(ctx context.Context, req *pb.Floor, res *pb.FloorResponse) error {
	log.Println(ctx)
	repo := s.GetRepo()
	defer repo.Close(ctx)

	mdata, ok := metadata.FromContext(ctx)

	var requestID string
	if ok {
		requestID = mdata["requestid"]
	}

	floorID := req.Id
	floor, err := repo.GetFloor(ctx, floorID)
	if err != nil {
		log.Printf("Error: Failed to get floor with ID %s : %+v", floorID, err)
		return err
	}
	log.Printf("Method: GetFloor, RequestID: %s, Return: %+v", requestID, floor)
	res.Floor = floor
	return nil
}
